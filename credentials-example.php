// AUTH0 CREDENTIALS
defined("AUTH0_DOMAIN") or define("AUTH0_DOMAIN", "");
defined("AUTH0_CLIENTID") or define("AUTH0_CLIENTID", "");
defined("AUTH0_SECRET") or define("AUTH0_SECRET", "");

// BLOOMFIRE CREDENTIALS
defined("BF_DOMAINS") or define("BF_DOMAINS", serialize (array ()));
defined("BF_BASE_URI") or define("BF_BASE_URI", "");
defined("BF_API_KEY") or define("BF_API_KEY", "");
defined("BF_SYSTEM_EMAIL") or define("BF_SYSTEM_EMAIL", "");
