<?php
/*
 * Connectors
 * Base class used for additional connectors
 */

require_once 'connector-auth0.php';
require_once 'connector-bloomfire.php';

class Connector {
    // Fetch content via API
    public function fetch($url, $params = array(), $type = 'GET', $headers = array()) {
      // The request is a POST
      if (strtoupper($type) === 'POST' || strtoupper($type) === 'PUT') {
        $ch = curl_init($url);

        if (is_array($params)) {
          curl_setopt($ch, CURLOPT_POST, count($params));
          $params = http_build_query($params);
        }

        if (strtoupper($type) === 'PUT') {
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

      // Default to a GET request
      } else {
        $ch = curl_init($url . '?' . http_build_query($params));
      }

      if (!empty($headers)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      }

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response = curl_exec($ch);

      return $response;
    }

    public function logout() {
      return false;
    }

    public function redirect($url) {
      return false;
    }

    public function setHeader($val) {
      header($val);
      return false;
    }

    public function set($key, $val) {
      if(!isset($_SESSION)){
        @session_start();
      }

      $_SESSION[$key] = $val;
    }

    public function get($key) {
      if(!isset($_SESSION)){
        @session_start();
      }

      return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
    }

    public function destroy($key) {
      if (isset($_SESSION[$key])) {
        unset($_SESSION[$key]);
      }
    }

    public function createUser($email = null, $domains = array(), $data = array()) {
      return false;
    }

    public function updateUser($email = null, $domains = array(), $data = array()) {
      return false;
    }

    public function removeUser($email = null, $domains = array()) {
      return false;
    }

    public function addUserToGroups($email = null, $reflectors = array()) {
      return false;
    }

    public function removeUserFromGroups($email = null, $reflectors = array()) {
      return false;
    }
}
