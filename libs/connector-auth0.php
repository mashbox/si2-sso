<?php
/*
 * Connector: Auth0
 * Description: functions to connect with Bloomfire
 */

require_once 'connectors.php';

class Auth0 extends Connector {

    public function getToken($user = null, $pass = null) {
      $url = 'https://' . AUTH0_DOMAIN . '/oauth/ro';
      $params = array(
        'username'      => $user,
        'password'      => $pass,
        'scope'         => 'openid offline_access',
        'grant_type'    => 'password',
        'connection'    => 'si2-website',
        'client_id'     => AUTH0_CLIENTID,
        'device'        => 'my-device',
      );

      // If token does not exist
      if (!$this->get('id_token') || ($user && $pass)) {
        if ($user && $pass) {
          $response = json_decode($this->fetch($url, $params, 'POST'));
          if (isset($response->id_token)) {
            $this->set('id_token', $response->id_token);
            $this->set('refresh_token', $response->refresh_token);
          } else {
            throw new Exception($response->error);
          }
        } else {
          throw new Exception('User is not currently logged in');
        }
      }

      return $this->get('id_token');
    }

    public function refreshToken() {
      if ($this->get('refresh_token')) {
        $url = 'https://' . AUTH0_DOMAIN . '/delegation';
        $params = array(
          'grant_type'    => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
          'refresh_token'     => $this->get('refresh_token'),
          'client_id'     => AUTH0_CLIENTID,
          'api_type'      => 'app',
        );

        $response = json_decode($this->fetch($url, $params, 'POST'));

        if (isset($response->id_token)) {
          $this->set('id_token', $response->id_token);
        } else {
          throw new Exception($response->error);
        }
      }

      return $this->get('id_token');
    }

    public function logout() {
      $this->destroy('id_token');
      $this->destroy('refresh_token');
    }

    public function userInfo() {
      $url = 'https://' . AUTH0_DOMAIN . '/tokeninfo';
      $params = array(
        'id_token' => $this->getToken()
      );

      $response = json_decode($this->fetch($url, $params, 'POST'));

      if (!isset($response) || (isset($response->error) && $response->error == 'invalid_token')) {
        try {
          $params = array(
            'id_token' => $this->refreshToken()
          );
          $response = json_decode($this->fetch($url, $params, 'POST'));

        } catch (Exception $e) {
          throw new Exception('Unable to get the user information');
        }
      }

      return $response;
    }


}
