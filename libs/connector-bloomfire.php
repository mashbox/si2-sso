<?php
/*
 * Connector: Bloomfire
 * Description: functions to connect with Bloomfire
 */

require_once 'connectors.php';

class Bloomfire extends Connector {

    // Redirect a url that matches
    public function redirect($url) {
      $domains = unserialize (BF_DOMAINS);
      array_push($domains, BF_BASE_URI);
      $found = false;

      foreach ($domains as $domain) {
        if (strpos(strtolower($url), strtolower($domain)) > -1) {
          $found = true;
        }
      }

      if ($found) {
        $purl  = parse_url($url);
        $base_uri = $purl['scheme'] . '://' . $purl['host'];
        $qs = array();
        if (isset($purl['query'])) {
          parse_str($purl['query'], $qs);
        }
        $query = http_build_query(array_merge($qs, array('token' => $this->getToken($purl['host']))));
        $final =  $base_uri . (isset($purl['path']) ? $purl['path'] : '') . '?' . $query;
        return $this->setHeader('Location: ' . $final);
      }
    }

    // Get the token for the user
    public function getToken($domain = null) {
      $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
      $url = $base_uri . '/api/v2/login';
      $params = array(
        'email' => $this->get('bf_email'),
        'fields' => 'login_token',
        'api_key' => BF_API_KEY,
      );

      $response = json_decode($this->fetch($url, $params));

      if (!isset($response->login_token)) {
        throw new Exception('Unable to get token');
      }

      return $response->login_token;
    }

    // Get the session token for the admin user
    public function getSessionToken($domain = null) {
      $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
      $url = $base_uri . '/api/v2/login';
      $params = array(
        'email' => BF_SYSTEM_EMAIL,
        'api_key' => BF_API_KEY,
      );

      $response = json_decode($this->fetch($url, $params));

      if (!isset($response->session_token)) {
        throw new Exception('Unable to get session token');
      }

      return $response->session_token;
    }

    // Create a user
    public function createUser($email = null, $domains = array(), $data = array()) {
      $data['active'] = true;

      return $this->updateUser($email, $domains, $data);
    }

    // Create a new user or update an existing user
    public function updateUser($email = null, $domains = array(), $data = array(), $remove = false) {

      foreach ($domains as $domain) {
        $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
        $url = $base_uri . '/api/v2/users';

        // Inject email into data
        $data['email'] = $email;

        // Get Session Token
        $session_token = $this->getSessionToken($domain);

        // Append API Key to user
        $url .= '?session_token=' . $session_token;

        $content = json_encode($data);
        $response = json_decode($this->fetch($url, $content, 'POST', array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($content)
        )));

        if (!isset($response->email)) {
          throw new Exception('Unable to update user');
        }
      }

      // Remove from all others
      if ($remove) {
        $all_domains = unserialize (BF_DOMAINS);
        $diff = array_diff($all_domains, $domains);

        foreach ($diff as $domain) {
          try {
            $r = $this->getUser($email, $domain);

            if ($r->email) {
              $this->deactivateUser($email, array($domain));
            }
          } catch (Exception $e) {
          }
        }
      }

      return $email;

    }

    // get a user
    public function getUser($email = null, $domain = null) {
      $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
      $url = $base_uri . '/api/v2/users';

      // Get Session Token
      $session_token = $this->getSessionToken($domain);

      // Append API Key to user
      $url .= '?session_token=' . $session_token;

      $data = json_encode(array(
        'email' => $email
      ));
      $response = json_decode($this->fetch($url, $data, 'POST', array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get user');
      }

      return $response;
    }

    // Deactivate a user
    public function deactivateUser($email = null, $domains = array()) {
      $request = $this->updateUser($email, $domains, array(
        'active' => false
      ));

      if (!$request) {
        throw new Exception('Unable to remove user');
      }

      return true;
    }

    // Remove a user
    public function removeUser($email = null, $domains = array()) {
      return $this->deactivateUser($email, $domains);
    }

    // Get Groups By Domain
    public function getGroupsByDomain($domain = null) {
      $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
      $url = $base_uri . '/api/v2/users/me';
      $groups = array();

      // Get Session Token
      $session_token = $this->getSessionToken($domain);

      $params = array(
        'session_token' => $session_token,
        'fields' => 'organizations(host,id,name,description,community(id,name,community_name,host),my_groups(host,id,name,description))',
      );

      $response = json_decode($this->fetch($url, $params));

      if (!isset($response)) {
        throw new Exception('Unable to get groups');
      }

      foreach ($response->organizations as $group) {
        if ($group->community->host == $domain) {
          array_push($groups, array(
            'id' => $group->id,
            'name' => $group->name,
            'host' => $group->host,
            'description' => $group->description,
          ));
          $groups = array_merge($groups, $group->my_groups);
        }
      }

      return $groups;
    }

    // Get Groups
    public function getGroupsByReflector($reflectors = array()) {
      $base_uri = BF_BASE_URI;
      $url = $base_uri . '/api/v2/users/me';
      $patterns = array();
      $groups = array();

      // Get Session Token
      $session_token = $this->getSessionToken();

      $params = array(
        'session_token' => $session_token,
        'fields' => 'organizations(host,id,name,description,community(id,name,community_name,host),my_groups(host,id,name,description))',
      );

      $response = json_decode($this->fetch($url, $params));

      if (!isset($response)) {
        throw new Exception('Unable to get groups');
      }

      // Build an array of reflector patterns
      foreach($reflectors as $reflector) {
        array_push($patterns, '/reflector:\s?' . $reflector .'/');
      }

      foreach($response->organizations as $organization) {

        // first check the description of the organization for a match
        foreach($patterns as $pattern) {
          $results = $this->matchPattern($pattern, $organization->description);

          if ($results) {
            array_push($groups, array(
             'id' => $organization->id,
             'name' => $organization->name,
             'host' => $organization->host,
             'description' => $organization->description,
           ));
          }
        }

        // Now check each of the groups in the organization
        foreach($organization->my_groups as $group) {
          foreach($patterns as $pattern) {
            $results = $this->matchPattern($pattern, $group->description);

            if ($results) {
              array_push($groups, array(
                'id' => $group->id,
                'name' => $group->name,
                'host' => $group->host,
                'description' => $group->description,
              ));
            }
          }
        }
      }

      return $groups;
    }

    // Match a regex pattern
    private function matchPattern($pattern, $subject) {
      $results = false;

      if (!empty($subject)) {
        preg_match($pattern, $subject, $matches);
        if ($matches) {
          $results = true;
        }
      }

      return $results;
    }

    // Add a user to multiple groups
    public function addUserToGroups($email = null, $reflectors = array()) {
      if (empty($email)) {
        throw new Exception('A user email is required');
      }

      if (empty($reflectors)) {
        throw new Exception('Reflectors are required');
      }

      // Get the reflector Groups
      $groups = $this->getGroupsByReflector($reflectors);

      foreach($groups as $group){
        // First, get the user ID
        $user = $this->getUser($email, $group['host']);

        if (!isset($user->id)) {
          throw new Exception('No user with that email found');
        }
        $this->addUserToGroup($group['host'], $user->id, $group['id']);
      }

      return true;
    }

    // Add a user to a group
    public function addUserToGroup($domain = null, $user = null, $group = null, $level = 'learner') {
      if (empty($user)) {
        throw new Exception('A user ID is required');
      }

      if (empty($group)) {
        throw new Exception('A Group ID is required');
      }

      $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
      $community_admins = array();
      $community_authors = array();
      $community_learners = array();

      // Get Session Token
      $session_token = $this->getSessionToken($domain);

      // First get all of the existing admins
      $url = $base_uri . '/api/v2/groups/' . $group . '/community_admins';
      $response = json_decode($this->fetch($url, array(
        'fields' => 'id',
        'session_token' => $session_token
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get admins');
      }

      foreach ($response as $admin) {
        array_push($community_admins, $admin->id);
      }

      // Next get all of the existing authors
      $url = $base_uri . '/api/v2/groups/' . $group . '/community_authors';
      $response = json_decode($this->fetch($url, array(
        'fields' => 'id',
        'session_token' => $session_token
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get authors');
      }

      foreach ($response as $author) {
        array_push($community_authors, $author->id);
      }

      // Next get all of the existing learners
      $url = $base_uri . '/api/v2/groups/' . $group . '/community_learners';
      $response = json_decode($this->fetch($url, array(
        'fields' => 'id',
        'session_token' => $session_token
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get learners');
      }

      foreach ($response as $learner) {
        array_push($community_learners, $learner->id);
      }

      if ($level == 'admin') {
        array_push($community_admins, $user);
      } elseif ($level == 'author') {
        array_push($community_authors, $user);
      } elseif ($level == 'learner') {
        array_push($community_learners, $user);
      }

      // Add the user to the right group here
      array_push($community_learners, $user);

      // Create the new data
      $data = array(
        'id' => $group,
        'group_admin_ids' => $community_admins,
        'group_member_ids' => $community_authors,
        'group_learner_ids' => $community_learners,
      );

      // Append API Key to user
      $url = $base_uri . '/api/v2/groups/' . $group;
      $url .= '?fields=id&session_token=' . $session_token;
      $content = json_encode($data);
      $response = json_decode($this->fetch($url, $content, 'PUT', array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($content)
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get groups');
      }

      return true;
    }

    // remove a user from multiple groups
    public function removeUserFromGroups($email = null, $reflectors = array()) {
      if (empty($email)) {
        throw new Exception('A user email is required');
      }

      if (empty($reflectors)) {
        throw new Exception('Reflectors are required');
      }

      // Get the reflector Groups
      $groups = $this->getGroupsByReflector($reflectors);

      foreach($groups as $group){
        // First, get the user ID
        $user = $this->getUser($email, $group['host']);

        if (!isset($user->id)) {
          throw new Exception('No user with that email found');
        }
        $this->removeUserFromGroup($group['host'], $user->id, $group['id']);
      }

      return true;
    }

    // Remove a user from a group
    public function removeUserFromGroup($domain = null, $user = null, $group = null) {
      if (empty($user)) {
        throw new Exception('A user ID is required');
      }

      if (empty($group)) {
        throw new Exception('A Group ID is required');
      }

      $base_uri = isset($domain) ? 'https://' . $domain : BF_BASE_URI;
      $community_admins = array();
      $community_authors = array();
      $community_learners = array();

      // Get Session Token
      $session_token = $this->getSessionToken($domain);

      // First get all of the existing admins
      $url = $base_uri . '/api/v2/groups/' . $group . '/community_admins';
      $response = json_decode($this->fetch($url, array(
        'fields' => 'id',
        'session_token' => $session_token
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get admins');
      }

      foreach ($response as $admin) {
        if ($admin->id !== $user) {
          array_push($community_admins, $admin->id);
        }
      }

      // Next get all of the existing authors
      $url = $base_uri . '/api/v2/groups/' . $group . '/community_authors';
      $response = json_decode($this->fetch($url, array(
        'fields' => 'id',
        'session_token' => $session_token
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get authors');
      }

      foreach ($response as $author) {
        if ($author->id !== $user) {
          array_push($community_authors, $author->id);
        }
      }

      // Next get all of the existing learners
      $url = $base_uri . '/api/v2/groups/' . $group . '/community_learners';
      $response = json_decode($this->fetch($url, array(
        'fields' => 'id',
        'session_token' => $session_token
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get learners');
      }

      foreach ($response as $learner) {
        if ($learner->id !== $user) {
          array_push($community_learners, $learner->id);
        }
      }

      // Create the new data
      $data = array(
        'id' => $group,
        'group_admin_ids' => $community_admins,
        'group_member_ids' => $community_authors,
        'group_learner_ids' => $community_learners,
      );

      // Append API Key to user
      $url = $base_uri . '/api/v2/groups/' . $group;
      $url .= '?fields=id&session_token=' . $session_token;
      $content = json_encode($data);
      $response = json_decode($this->fetch($url, $content, 'PUT', array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($content)
      )));

      if (!isset($response)) {
        throw new Exception('Unable to get groups');
      }

      return true;
    }
}
