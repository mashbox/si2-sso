<?php
/*
 * Si2 Single Sign On
 * Description: Single sign on library
 */

require_once __DIR__ . '/libs/connectors.php';
require_once __DIR__ . '/credentials.php';

class SSO {
    private static $connectors;
    private static $auth0;

    // Constructor
    function __construct() {
      // Initialize Auth0
      self::$auth0 = new Auth0();

      // Instantiate Connectors
      self::$connectors = new stdClass();
      self::$connectors->bloomfire = new Bloomfire();
    }

    // login function
    public function login($user, $pass, $redirect = '', $save_session = false) {
      // First validate the user with Auth0
      $token = self::$auth0->getToken($user, $pass);

      // Login Successful
      if ($token) {
        // Check if session is set
        if(!isset($_SESSION)){
          session_start();
        }

        // If the session needs to be saved
        if ($save_session) {
          setcookie(session_name(),session_id(),time()+2592000);
        }

        // Create additional tokens
        try {
          $bf = self::$connectors->bloomfire;
          $bf::set('bf_email', $user);

        } catch(Exception $e) {
          throw new Exception('Unable to instantiate SSO connectors.');
        }

        // Figure out redirect
        if (isset($redirect) && !empty($redirect)) {
          $this->redirect($redirect);
        }

        return true;

      // Not Successful
      } else {
        throw new Exception('Unable to authenticate user');
      }
    }

    // logout function
    public function logout() {
      // Run logout through each connector
      foreach(self::$connectors as $connector){
        $connector->logout();
      }

      if (isset($_SESSION)) {
        setcookie(session_name(),null,-1);
      }
    }

    // redirect function
    public function redirect($url) {

      if (isset($url) && !empty($url)) {
        // Run redirect through each connector
        foreach(self::$connectors as $connector){
          $connector->redirect($url);
        }

        // Not a connector redirect, just forward the link
        header('Location: ' . $url);
        die();
      }
    }

    // is authenticated function
    public function isAuthenticated() {
      $auth = false;

      try {
        $token = self::$auth0->getToken();

        if (isset($token) && $token) {
          $auth = true;
        }
      } catch(Exception $e) {}

      return $auth;
    }

    // get profile function
    public function getProfile() {
      if ($this->isAuthenticated()) {
        try {
          $auth0 = self::$auth0;
          return $auth0->userInfo();

        } catch(Exception $e) {
          throw new Exception('Unable to get Profile');
        }
      }
    }

    // create user function
    public function createUser($email = null, $domains = array(), $data = array()) {

      // Make sure that email is passed
      if (empty($email)) {
        throw new Exception('No user email provided');
      }

      // When ready for Auth0 add here

      // Run create through each connector
      foreach(self::$connectors as $connector){
        try {
          return $connector->createUser($email, $domains, $data);
        } catch(Exception $e) {
          throw new Exception('Unable to create user');
        }
      }
    }

    // update user function
    public function updateUser($email = null, $domains = array(), $data = array()) {
      // When ready for Auth0 add here

      // Run create through each connector
      foreach(self::$connectors as $connector){
        try {
          return $connector->updateUser($email, $domains, $data);
        } catch(Exception $e) {
          throw new Exception('Unable to create user');
        }
      }
    }

    // remove user function
    public function removeUser($email = null, $domains = array()) {
      // When ready for Auth0 add here

      // Run create through each connector
      foreach(self::$connectors as $connector){
        try {
          return $connector->removeUser($email, $domains);
        } catch(Exception $e) {
          throw new Exception('Unable to remove user');
        }
      }
    }

    // Add a user to a workgroup
    public function addUserToGroups($email = null, $reflectors = array()) {

      // Make sure that email is passed
      if (empty($email)) {
        throw new Exception('No user email provided');
      }

      if (empty($reflectors)) {
        throw new Exception('No reflectors were specified');
      }

      // Run create through each connector
      foreach(self::$connectors as $connector){
        try {
          return $connector->addUserToGroups($email, $reflectors);
        } catch(Exception $e) {
          throw new Exception('Unable to add users');
        }
      }
    }

    // Remove a user from a workgroup
    public function removeUserFromGroups($email = null, $reflectors = array()) {

      // Make sure that email is passed
      if (empty($email)) {
        throw new Exception('No user email provided');
      }

      if (empty($reflectors)) {
        throw new Exception('No reflectors were specified');
      }

      // Run create through each connector
      foreach(self::$connectors as $connector){
        try {
          return $connector->removeUserFromGroups($email, $reflectors);
        } catch(Exception $e) {
          throw new Exception('Unable to remove user');
        }
      }
    }
}
