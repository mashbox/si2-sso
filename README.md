# Si2 Single Sign On Library #
*Version 0.2.1*

A library to help with the single sign-on integrations between the si2.org website and various other services.

Currently available connectors are:
* Auth0
* Bloomfire

## Installation ##
To install the library just copy this directory into the website sources.

Copy the credentials-example.php file and rename to credentials.php. Enter the appropriate credentials from the connectors into the available define statements.

## Reflectors ##
In order to use the reflector features the unique ID for the reflector group needs to be included in the Bloomfire group description on a new line, for example:

reflector: lpc_format@si2.org

## Functions ##
To utilize the SSO you can use the following functions to login, logout, check the current authentication, and get the current user's profile.

###login($user (String), $password (String), $redirect (String))###

Use the login function to send the user's credentials for validation. If the login is unsuccessful an error will be thrown containing the associated message, otherwise the user's profile will be returned from the function.

```
<?php
  try {
    // First you will need instantiate the SSO
    $sso = new SSO();

    // Then trigger login
    $user = $sso->login('username', 'password', 'http://path/to/my/page');
  } catch (Exception $e) {
    echo $e->getMessage();
  }
?>
```

###logout()###

Logout the currently active user and remove any associated tokens.

```
<?php
  $sso->logout();
?>
```

###redirect($url (String))###

Redirect a url through the connectors

```
<?php
  $sso = new SSO();
  $sso->redirect('http://is/my/custom/url');
?>
```

###isAuthenticated()###

Checks if a user is currently authenticated and returns a boolean.

```
<?php
  $auth = $sso->isAuthenticated();

  if ($auth) {
    echo 'User is Authenticated';

  } else {
    echo 'No user available.'
  }
?>
```

###getProfile()###

Checks if a user is currently authenticated and returns a boolean.

```
<?php
  $user = $sso->getProfile();
  echo 'Email: ' . $user->email;
?>
```

###createUser($email (String), $domains (Array), $metadata (Array))###

Creates a new user

```
<?php
  $user = $sso->createUser('john.doe@mysite.com', array(
    'community1',
    'community2',
  ), array(
    'first_name' => 'John',
    'last_name' => 'Doe'
  ));
  echo 'User: ' .  $user->email;
?>
```

###updateUser($email (String), $domains (Array), $metadata (Array), $remove (Bool))###

Updates an existing user, if the remove flag is true then the user will be removed from all other domains not sent.

```
<?php
  $user = $sso->updateUser('john.doe@mysite.com', array(
    'community3',
    'community4',
  ), array(
    'first_name' => 'John',
    'last_name' => 'Doe'
  ), false);
  echo 'User: ' .  $user->email;
?>
```

###removeUser($email (String), $domains (Array))###

Removes a user

```
<?php
  $user = $sso->removeUser('john.doe@mysite.com', array(
    'community2',
  ));
?>
```

###addUserToGroups($email (String), $reflectors (Array))###

Add a user to groups associated to a reflector

```
<?php
  $add = $sso->addUserToGroups('john.doe@mysite.com', array(
    'lpc_format@si2.org',
  ));
?>
```

###removeUserFromGroups($email (String), $reflectors (Array))###

Remove a user from groups associated to a reflector

```
<?php
  $del = $sso->removeUserFromGroups('john.doe@mysite.com', array(
    'lpc_format@si2.org',
  ));
?>
```

## Development ##
For development you should install the composer dependencies.

### Dependencies ###
* PHP  >= 5.5
* PHP Composer

### Change Log ###
* 0.2.1 - Added Group Management for Bloomfire
* 0.2.0 - Include CRUD for users, only Bloomfire is active
* 0.1.0 - Initial release
