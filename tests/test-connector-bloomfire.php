<?php
require_once __DIR__ . '/../credentials.php';
require_once __DIR__ . '/../libs/connector-bloomfire.php';

class ConnectorBloomfireTest extends PHPUnit_Framework_TestCase {

  public function testCanRedirect() {
    // Create a stub for the SomeClass class.
    $stub = $this->getMock('Bloomfire', array('setHeader', 'getToken'));

    // Configure the stub.
    $stub->method('setHeader')
         ->willReturn('Location: http://community.si2.org');

    $stub->method('getToken')
         ->willReturn('my_token');

    $rd = $stub->redirect('http://community.si2.org');

    $this->assertEquals('Location: http://community.si2.org', $rd);
  }

  public function testCanGetToken() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('fetch'));

    // Configure the stub.
    $stub->method('fetch')
        ->willReturn('{ "login_token": "my_token" }');

    $this->assertEquals('my_token', $stub->getToken('community.si2.org'));
  }

  public function testCanCreateUser() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "email": "test@test.com" }');

    $this->assertEquals('test@test.com', $stub->createUser('test@test.com', array('community.si2.org'), array('title' => 'tester')));
  }

  public function testCanUpdateUser() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "email": "test@test.com" }');

    $this->assertEquals('test@test.com', $stub->updateUser('test@test.com', array('community.si2.org'), array('title' => 'tester')));
  }

  public function testCanGetUser() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "email": "test@test.com", "active": "true" }');

    $this->assertEquals('test@test.com', $stub->getUser('test@test.com', 'community.si2.org')->email);
    $this->assertEquals('true', $stub->getUser('test@test.com', 'community.si2.org')->active);
  }

  public function testCanDeactivateUser() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "email": "test@test.com", "active": "false" }');

    $this->assertEquals(true, $stub->deactivateUser('test@test.com', array('community.si2.org')));
  }

  public function testCanRemoveUser() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "email": "test@test.com", "active": "false" }');

    $this->assertEquals(true, $stub->removeUser('test@test.com', array('community.si2.org')));
  }

  public function testCanGetGroupsByDomain() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "organizations": [{ "id": "1", "host": "test.domain.org", "name": "my name", "description": "my description", "community": { "host": "community.si2.org" }, "my_groups": [{ "id": "1", "host": "test.domain.org", "name": "my name", "description": "my description" }] }] }');

    $groups = $stub->getGroupsByDomain('community.si2.org');

    $this->assertEquals('1', $groups[0]['id']);
    $this->assertEquals('test.domain.org', $groups[0]['host']);
    $this->assertEquals('my name', $groups[0]['name']);
  }

  public function testCanGetGroupsByReflector() {
    $stub = $this->getMockBuilder('Bloomfire')
                 ->getMock();

    $stub = $this->getMock('Bloomfire', array('getSessionToken', 'fetch'));

    // Configure the stub.
    $stub->method('getSessionToken')
        ->willReturn('my_token');

    $stub->method('fetch')
        ->willReturn('{ "organizations": [{ "id": "1", "host": "test.domain.org", "name": "my name", "description": "reflector:test-reflector", "community": { "host": "community.si2.org" }, "my_groups": [{ "id": "1", "host": "test.domain.org", "name": "my name", "description": "my description" }] }] }');

    $groups = $stub->getGroupsByReflector(array('test-reflector'));

    $this->assertEquals('1', $groups[0]['id']);
    $this->assertEquals('test.domain.org', $groups[0]['host']);
    $this->assertEquals('my name', $groups[0]['name']);
  }

  public function testCanAddUserToGroups() {
    $this->assertEquals(1, true);
  }

  public function testCanAddUserToGroup() {
    $this->assertEquals(1, true);
  }

  public function testCanRemoveUserFromGroups() {
    $this->assertEquals(1, true);
  }

  public function testCanRemoveUserFromGroup() {
    $this->assertEquals(1, true);
  }
}
