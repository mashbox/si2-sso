<?php

require_once __DIR__ . '/../credentials.php';
require_once __DIR__ . '/../libs/connector-auth0.php';

class ConnectorAuth0Test extends PHPUnit_Framework_TestCase {
  public function testCanGetToken() {
    // Create a stub for the SomeClass class.
    $stub = $this->getMock('Auth0', array('fetch'));

    // Configure the stub
    $stub->method('fetch')
         ->willReturn('{ "id_token": "my_id_token", "refresh_token": "my_refresh_token"}');


    $this->assertEquals('my_id_token', $stub->getToken('user', 'pass'));
  }

  public function testCanRefreshToken() {
    // Create a stub for the SomeClass class.
    $stub = $this->getMock('Auth0', array('fetch'));

    // Configure the stub
    $stub->method('fetch')
         ->willReturn('{ "id_token": "my_id_token", "refresh_token": "my_refresh_token"}');


    $this->assertEquals('my_id_token', $stub->refreshToken('user', 'pass'));
  }

  public function testCanLogout() {
    $this->assertEquals(1, true);
  }

  public function testCanGetUserInfo() {
    // Create a stub for the SomeClass class.
    $stub = $this->getMock('Auth0', array('fetch'));

    // Configure the stub
    $stub->method('fetch')
         ->willReturn('{ "email": "test@test.com" }');


    $this->assertEquals('test@test.com', $stub->userInfo()->email);
  }
}
