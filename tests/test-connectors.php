<?php
class ConnectorTest extends PHPUnit_Framework_TestCase {

  public function testCanFetch() {
    // Test the Fetch mechanism
  }

  public function testCanLogout() {
    $connector = new Connector();
    $this->assertEquals($connector->logout(), false);
  }

  public function testCanRedirect() {
    $connector = new Connector();
    $this->assertEquals($connector->redirect('http://www.google.com'), false);
  }

  /**
   * @runInSeparateProcess
   */
  public function testCanSet() {
    $connector = new Connector();
    $connector->set('mysetsession', 'taco');

    $this->assertEquals($_SESSION['mysetsession'], 'taco');
  }

  /**
   * @runInSeparateProcess
   */
  public function testCanGet() {
    $connector = new Connector();
    $connector->set('mygetsession', 'burrito');

    $this->assertEquals($connector->get('mygetsession'), 'burrito');
  }

  /**
   * @runInSeparateProcess
   */
  public function testCanDestroy() {
    $connector = new Connector();
    $connector->set('mydestroysession', 'burrito');

    $this->assertEquals($connector->get('mydestroysession'), 'burrito');

    $connector->destroy('mydestroysession');

    $this->assertEquals($connector->get('mydestroysession'), null);
  }
}
