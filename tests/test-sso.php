<?php
class SSOTest extends PHPUnit_Framework_TestCase {
  public function testCanLogin() {
    $this->assertEquals(1, true);
  }

  public function testCanLogout() {
    $this->assertEquals(1, true);
  }

  public function testCanRedirect() {
    $this->assertEquals(1, true);
  }

  public function testIsAuthenticated() {
    $this->assertEquals(1, true);
  }

  public function testCanGetProfile() {
    $this->assertEquals(1, true);
  }

  public function testCanCreateUser() {
    $this->assertEquals(1, true);
  }

  public function testCanUpdateUser() {
    $this->assertEquals(1, true);
  }

  public function testCanRemoveUser() {
    $this->assertEquals(1, true);
  }

  public function testCanAddUserToGroups() {
    $this->assertEquals(1, true);
  }

  public function testCanRemoveUserUserFromGroups() {
    $this->assertEquals(1, true);
  }
}
