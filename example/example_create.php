<?php
  include_once('../sso.php');

  $sso = new SSO();

  $return_to = $_REQUEST['return_to'];
  if (isset($return_to) && !preg_match('@https?@i', $return_to)) {
    print preg_replace("/^(\/)/i", "https://", $return_to);
  }

  // Example Create function
  if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'create') {
    $metadata = array(
      'send_welcome_email' => true,
    );

    try {
      if (!empty($_REQUEST['email'])) {
        $response = $sso->createUser($_REQUEST['email'], array('community.si2.org', 'openstandards.si2.org'), $metadata);
        echo '<p style="color: green;text-align: center;font-weight: bold;">User ' . $response . ' was created</p>';
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }

  // remove
  } else if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'remove') {
    try {
      if (!empty($_REQUEST['email'])) {
        $response = $sso->removeUser($_REQUEST['email'], array('community.si2.org', 'openstandards.si2.org'));

        echo '<p style="color: red;text-align: center;font-weight: bold;">User ' . $_REQUEST['email'] . ' was deleted</p>';
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
?>
<!doctype html>
<!--[if IE 9]>    <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Si2 SSO Example</title>
  <meta name="description" content="">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <div class="col-sm-4 col-md-offset-4">
      <form id="form-id" method="post">
        <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control" id="email" name="email" value="dev+test@mashbox.com">
        </div>
        <input type="hidden" id="action" name="action" value="">
        <input type="button" class="btn btn-default signin-db" onclick="document.getElementById('action').value = 'create';document.getElementById('form-id').submit();" value="Create"/>
        <input type="button" class="btn btn-default signin-db" onclick="document.getElementById('action').value = 'remove';document.getElementById('form-id').submit();" value="Remove"/>
      </form>
    </div>
  </div>
</body>
</html>
