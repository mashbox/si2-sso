<?php
  include_once('../sso.php');

  $sso = new SSO();

  // Example Login function
  if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'login') {
    $user = isset($_REQUEST['email']) ? $_REQUEST['email'] : null;
    $pass = isset($_REQUEST['password']) ? $_REQUEST['password'] : null;
    $redirect = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : null;

    try {
      if ($user && $pass) {
        $sso->login($user, $pass, $redirect, true);
      }
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }

  if (isset($_REQUEST['return_to']) && $_REQUEST['return_to']) {
    if (!preg_match('%redirected=true%', $_REQUEST['return_to'])) {
      $sso->redirect($_REQUEST['return_to']);
    } else {
      print "REDIRECT!";
    }
  }
?>
<!doctype html>
<!--[if IE 9]>    <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Si2 SSO Example</title>
  <meta name="description" content="">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <div class="col-sm-4 col-md-offset-4">
      <?php
        try {
          $auth = $sso->isAuthenticated();

          if (isset($auth) && $auth) {
            echo '<p style="color: green;text-align: center;font-weight: bold;">User is currently Authenticated</p>';

            $user = $sso->getProfile();

            if ($user) {
              echo '<p style="color: green;text-align: center;font-weight: bold;">Email: ' . $user->email . '</p>';
            }
          }
        } catch(Exception $e) {
          echo '<p style="color: red;text-align: center;font-weight: bold;">Email: ' . $e->getMessage() . '</p>';
        }
      ?>
      <form method="post">
        <div class="form-group">
          <label>Email</label>
          <input type="text" class="form-control" id="email" name="email" value="dev@mashbox.com">
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" class="form-control" id="password" name="password" value="testpassword">
        </div>
        <input type="hidden" name="action" value="login">
        <!-- Test out a standard Link -->
        <input type="hidden" name="redirect" value="">
        <!-- Test out a Bloomfire link -->
        <!-- <input type="hidden" name="redirect" value="https://home.si2.org/posts/1056888-references-agile-productdevelopement"> -->
        <input type="submit" class="btn btn-default signin-db" value="Sign in"/>
      </form>
    </div>
  </div>
</body>
</html>
